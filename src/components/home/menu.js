import React, {Component} from 'react';
import ReactDOM from 'react-dom';
import '../../stylesheets/carousel_config.min.css'; // requires a loader
import { Carousel } from 'react-responsive-carousel';
import '../../stylesheets/menu.scss';
import '../../stylesheets/carousel.scss';


class Menu extends React.Component{
 constructor(props){

  super(props);
  this.state = {
       products: []
       };
       console.log(this.props.value)
       this.loadProducts = this.loadProducts.bind(this);
    }

    async loadProducts() {
       let response = await fetch('http://localhost:3001/products');
       const products = await response.json();
       this.setState({ products: products });
       }
     
     componentDidMount() {
       this.loadProducts();
     }
  
  render() {
    return(
      <div className="menu" id="menu">
        <div className="menuCarousel">
          <Carousel showThumbs={false} showStatus={false} useKeyboardArrows>
          {this.state.products.map((product) => (
            <div className="carouselItem">
              <div className="carouselItem--left">
                <div className="productTitle"><div className="productFlavor"> {product.flavor}</div></div>
                <div className="productData">
                    <div className="productDescription">{product.description}</div>
                    <div className="nutritionalInfo">
                      <div className="nutritionalInfo_item">
                        <div className="nutritionalInfo_item--title">Qual o peso?</div>
                      {product.weight} gramas
                      
                      </div>
                      <div className="nutritionalInfo_item">
                       <div className="nutritionalInfo_item--title">E o valor calórico?</div>
                      {product.value_cal} kcal/gr

                      </div>
                      <div className="nutritionalInfo_item">
                      <div className="nutritionalInfo_item--title">Tem versão vegana?</div>
                      {product.vegan == true ? "sim :)" : "não :("}

                      </div>
                      <div className="nutritionalInfo_item">
                      <div className="nutritionalInfo_item--title">Tem versão gluten-free?</div>
                      {product.gluten_free == true ? "sim :)" : "não :("}

                      </div>
                      <div className="nutritionalInfo_item">
                      <div className="nutritionalInfo_item--title">E versão orgânica?</div>
                      {product.organic == true ? "sim :)" : "não :("}

                      </div>
                    </div>
                </div>
                <div className="">
                  <div className="">FAÇA JÁ SEU PEDIDO</div>
                </div>
              </div>
              <div className="carouselItem--right">
                <img src={require(`../../stylesheets/${product.img}.png`)}/>
            
              </div>
            </div>
          ))}
          </Carousel>
        </div>
      </div>
    );
  }
}

export default Menu;

