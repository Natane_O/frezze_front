import React, {Component} from 'react';
import ReactDOM from 'react-dom';
import { parseISO, getDate, addHours } from 'date-fns';
import NewOrder from '../orders/newOrder';
import '../../stylesheets/home.scss';
import '../../stylesheets/actionbutton.scss';


class Index extends React.Component{
   constructor(props) {
       super(props);
       this.state = {
       };
    }

  
  render() {
    return (
      <div id="index">
      <HomeContent/>
      </div>
    );
  }
}

class HomeContent extends React.Component{
  constructor(props){

  super(props);
  this.state = {
      show: false,
       orders: []
       };
       console.log(this.props.value)
      this.handleChildUnmount = this.handleChildUnmount.bind(this);
       this.loadOrders = this.loadOrders.bind(this);
    }

    async loadOrders() {
       let response = await fetch('http://localhost:3001/orders');
       const orders = await response.json();
       this.setState({ orders: orders });

       orders.filter(order => {
        const parsedDate = parseISO(order.delivery_time);
        console.log(parsedDate);
        console.log(new Date())
       })
     }
     
     componentDidMount() {
       this.loadOrders();
     }
  
  showModal() {
    this.setState({ show: !this.state.show });
  };

  handleChildUnmount() {
    this.setState({ show: false });
  }

  render() {
    return (
      <div className="home">
        <div className="homeContent">
          <div className="homeContent--left">
            <div>pizzas prontas de hoje{this.state.orders.filter((order) => order.status == "done" && getDate( parseISO(order.delivery_time)) === getDate(addHours(new Date(),3))).length}</div>
            <div>pizzas agendadas pra hoje{this.state.orders.filter((order) => getDate( parseISO(order.delivery_time)) === getDate(addHours(new Date(),3))).length}</div>
            <div>pizzas por usuario {this.state.orders.filter((order) => order.user_id == "1" ).length}</div>
          </div>
          <div className="homeContent--center">
            <img className="image" src={require('../../stylesheets/plate.png')} />
          </div>
          <div className="homeContent--right">
            <div>total de pizzas {this.state.orders.length}</div>
            <div>pizzas sendo preparadas {this.state.orders.filter((order) => order.status == "preparing" ).length}</div>
          </div>
        </div>
        <div className="actionButton">
          <div onClick={e => {this.showModal()}} className="button">FAÇA JÁ SEU PEDIDO</div>
        </div>
        {this.state.show ? <NewOrder unmountMe={this.handleChildUnmount} /> : null}
      </div>
    );
  }
}




export default Index;

