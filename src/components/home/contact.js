import React, {Component} from 'react';
import ReactDOM from 'react-dom';
import '../../stylesheets/contact.scss';

class Contact extends React.Component{
  render() {
    return (
      <div className="contact" id="contact">
        <div className="aboutContent">
          <div className="aboutTitle">Contato</div>
         
            <form   className="formContact">
            <div className="formItem">
              <label>Nome:</label>
              <input type="text" name="name" />
              </div>
              <div className="formItem">
              <label> Email:</label>
                <input type="email" name="email" />
              </div>
              <div className="formItem">
              <label>
                Mensagem:</label>
                <textarea type="text" name="message" />  
                </div>
                <div>
              <input type="submit" value="Enviar" />
              </div>
            </form>
        
        </div>
      </div>
    );
  }
}

export default Contact;

