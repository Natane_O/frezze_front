import React, {Component} from 'react';
import ReactDOM from 'react-dom';
import '../../stylesheets/about.scss';

class About extends React.Component{
  render() {
    return (
      <div className="about" id="about">
        <div className="aboutContent">
          <div className="aboutTitle">Quem somos?</div>
          <div className="aboutDescription">Somos uma pizzaria especializada em fazer pizzas para pessoas felizes! Aqui são produzidas pizzas saborosas em um ambiente 100% acolhedor. Nossa motivação é fazer pizzas incríveis e que nossos clientes fiquem com a sensação de "quero mais".
            Na Pizza Oven você pode comer sua pizza (independente de qualquer restrição alimentar,sem culpa e se divertindo). Criamos um cardápio diversificado, tipicamente brasileiro, com massas muito saborosas. Já experimentou?
            Experimente e seja feliz nesse mundo,
          </div>
          <div className="catchPhrase">afinal, tudo termina em pizza!</div>
        </div>
      </div>
    );
  }
}

export default About;

