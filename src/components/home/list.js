import React, {Component} from 'react';
import ReactDOM from 'react-dom';
import '../../stylesheets/about.scss';
import '../../stylesheets/modal/list.scss';

class List extends React.Component{
  constructor(props){

  super(props);
  this.state = {
       orders: []
       };
       this.loadOrders = this.loadOrders.bind(this);
    }

    async loadOrders() {
       let response = await fetch('http://localhost:3001/orders');
       const orders = await response.json();
       this.setState({ orders: orders });
       console.log(orders)
     }
     
     componentDidMount() {
       this.loadOrders();
     }
  
    
  render() {
    return (
      <div className="list_Modal">
        <div className="fog">
          <div className="list_container">
            <div className="list_content">
              <table>
                <tr>
                  <th>Número de pedido</th>
                  <th>ID Usuario</th>
                  <th>Data de entrega</th>
                  <th>status</th>
                  <th>Observações</th>
                  <th>Endereço de Entrega</th>
                </tr>
                {this.state.orders.map((order, index) => (
                  <tr>
                    <td>{index}</td>
                    <td>{order.user_id}</td>
                    <td>{order.delivery_time}</td>
                    <td>{order.status}</td>
                    <td>{order.comments}</td>
                    <td>{order.delivery_address}</td>
                  </tr>
                ))}
              </table>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

export default List;

