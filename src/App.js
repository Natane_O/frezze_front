import React from 'react';
import logo from './logo.svg';
import './App.scss';
import Header from './components/shared/header';
import Index from './components/home/index';
import About from './components/home/about';
import Menu from './components/home/menu';
import Contact from './components/home/contact';


function App() {
  return (
    <div>
    <Header/>
    <Index/>
    <About/>
    <Menu/>
    <Contact/>
    </div>
  );
}

export default App;
