import React, {Component} from 'react';
import ReactDOM from 'react-dom';
import '../../stylesheets/modal/newOrder.scss';


class NewOrder extends React.Component{
   constructor(props) {
    super(props);
    this.state = {
      PizzaChoose: false,
      PizzaData: false,
      UserData: false,
      DeliveryData: false,
      values: {}
    };
    this.controlPage = this.controlPage.bind(this);
  }

  controlPage(current, next, values = null) {
    if (values && this.state.values) {
      this.setState({ values: Object.assign(this.state.values, values) });
    } else if (values) {
      this.setState({ values: values });
    }
    this.setState({ [current]: false, [next]: true });
  }

  componentDidMount() {
    this.setState({
      PizzaChoose: true
    });
  }



  render() {
    return (
      <div className="newOrder_Modal">
       <div className="fog">
       <div className="newOrder_container">
        <div className="newOrder_content">
          {this.state.PizzaChoose ? <PizzaChoose values={this.state.values} nextProp={this.controlPage} /> : null}
          {this.state.PizzaData ? <PizzaData values={this.state.values} nextProp={this.controlPage} /> : null}
          {this.state.UserData ? <UserData values={this.state.values} nextProp={this.controlPage} /> : null}
          {this.state.DeliveryData ? <DeliveryData values={this.state.values} nextProp={this.controlPage} /> : null}
        </div>
        </div>
       </div>
      </div>
    );
  }
}


class PizzaChoose extends React.Component{
  render() {
    return (
        <div className="pizzaChoose">
          <div className="title text--extraLarge">
            Novo pedido
          </div>
          <div className="newOrder_data">
          <div className="ask text--medium">Diz pra gente: quantas pizzas você deseja?</div>
          <input type="text"></input>
          <div className="ask text--medium">E qual formato das suas pizzas?</div>
          <div className="chooseProduct--objects">
            <div className="object"></div>
            <div className="object"></div>
          </div>
          <div onClick={(e) => this.props.nextProp("PizzaChoose", "PizzaData")} className="newOrder--actions"> Prosseguir > </div>
          </div>
          </div>
      )
  }
}

class PizzaData extends React.Component{
  render() {
    return (
        <div className="pizzaChoose">
          <div className="title text--extraLarge">
            Lorem Ipsum Dolor sit
          </div>
          <div className="ask text--medium">Quantas pizzas você deseja?</div>
          <input type="text"></input>
          <div className="ask text--medium">Qual formato das suas pizzas?</div>
          <div className="chooseProduct--objects">
            <div className="object"></div>
            <div className="object"></div>
          </div>
          <div onClick={(e) => this.props.nextProp("PizzaData", "UserData")} className="newOrder--actions"> Prosseguir > </div>
        </div>
      )
  }
}

class UserData extends React.Component{
  render() {
    return (
        <div className="pizzaChoose">
          <div className="title text--extraLarge">
            llllllllllllll
          </div>
          <div className="ask text--medium">Quantas pizzas você deseja?</div>
          <input type="text"></input>
          <div className="ask text--medium">Qual formato das suas pizzas?</div>
          <div className="chooseProduct--objects">
            <div className="object"></div>
            <div className="object"></div>
          </div>
          <div onClick={(e) => this.props.nextProp("UserData", "DeliveryData")} className="newOrder--actions"> Prosseguir > </div>
          </div>
      )
  }
}


class DeliveryData extends React.Component{
  render() {
    return (
        <div className="pizzaChoose">
          <div className="title text--extraLarge">
            llllllllllllll
          </div>
          <div className="ask text--medium">Quantas pizzas você deseja?</div>
          <input type="text"></input>
          <div className="ask text--medium">Qual formato das suas pizzas?</div>
          <div className="chooseProduct--objects">
            <div className="object"></div>
            <div className="object"></div>
          </div>
          <div className="newOrder--actions"> Prosseguir > </div>
          </div>
      )
  }
}


export default NewOrder;

