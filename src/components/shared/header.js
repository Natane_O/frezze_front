import React, {Component} from 'react';
import ReactDOM from 'react-dom';
import List from '../home/list';
import '../../stylesheets/header.scss';
import {
  BrowserRouter as Router,
  Switch,
  Route
} from "react-router-dom";
import { HashLink as Link } from 'react-router-hash-link';

class Header extends React.Component{
 constructor(props){
  super(props);
  this.state = {
      show: false
    }
  }

  showModal() {
    this.setState({ show: !this.state.show });
  };

  handleChildUnmount() {
    this.setState({ show: false });
  }
  render() {
    return (
      <div className="header">
        <div className="headerContent">
        <Router>
          <div className="headerContent__item">
            <Link to="../home/index#index">
              HOME
            </Link>
          </div>
          <div className="headerContent__item">
            <Link to="../home/about#about">
              ABOUT
            </Link>
          </div>
          <div className="headerContent__item">
             <img className="image" src={require('../../stylesheets/logo.png')} />
          </div>
          <div className="headerContent__item"> 
            <Link to="../home/menu#menu">
              MENU
            </Link></div>
          <div className="headerContent__item">
            <Link to="../home/contact#contact">
              CONTACT
            </Link>
          </div>
          
          </Router>
        </div>
         <div  onClick={e => {this.showModal()}} className="headerContent__list">
              Lista de pedidos
          </div>
         {this.state.show ? <List unmountMe={this.handleChildUnmount} /> : null}
      </div>
    );
  }
}



export default Header;

